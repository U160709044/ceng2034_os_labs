import os
import shutil
import magic #you need to install this library by typing pip install python-magic
import pdb


def form_folder_names():

	file_types = []
	for file in os.listdir('.'):
		try:
			file_types.append(magic.from_file(file, mime=True).split('/')[1])
		except:
			pass
	for type in file_types:
		try:
			os.makedirs(type + 'Folder')
		except:
			pass


def copy_into_appropiate_folder(file):
	try:
			type = magic.from_file(file, mime=True).split('/')[1]

			shutil.copy(file, os.path.join(type + "Folder", file))
			os.system("rm file*")
			os.system("mv files fileCategories")
	except Exception as e:
			pass


if __name__ == "__main__":

	form_folder_names()
	for file in os.listdir('.'):
		copy_into_appropiate_folder(file)
