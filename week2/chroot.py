import os,shutil,subprocess

directory_to_copy = '/bin/'
#to copy bash program
shutil.copytree(directory_to_copy, os.environ['HOME']+'/jail/bin', True)
os.mkdir(os.environ['HOME']+'/jail/lib')

#to copy dependencies to jail.
for line in subprocess.check_output('ldd /bin/bash',shell=True).splitlines():
    starting_index_of_path = line.find('/')
    finishing_index_of_path = line.find('(') - 1
    file_to_copy = line[starting_index_of_path:finishing_index_of_path]
    if(file_to_copy != ''):
        shutil.copy(file_to_copy,os.environ['HOME']+'/jail/lib')

os.chroot(os.environ['HOME'] + '/jail')